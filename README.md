Le livre est disponible à cette adresse : [Le Site](http://nicolas.saby.pages.mia.inra.fr/soilserv-dsmart/)

Pour compiler le livre, il est nécessaire d'ajouter un fichier nomme `.Rprofile` contenant l'initilalisation d'une variable où se trouve les données :

repdata = 'D:/projets/gsm.dsm/SoilServ/analyse/soilserv/livrables/dsmart/data'

Il est possible de ne pas jouer à chaque compilation le dsmart en initialisant la variable `JouerDSMART` du fichier 02-Jouerdsmart.Rmd à `FALSE`

# Objectif

Ce projet pour partager un protocole d'application de dsmart sur les données IGCS avec validatoin externe probabiliste
